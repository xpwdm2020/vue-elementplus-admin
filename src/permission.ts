import router from './router/index'
import store from './store/index'
import {getToken} from "@/utils/store";
router.beforeEach((to, from, next) => {
    if(getToken()){
        if(to.path == '/login'){
            next("/")
        }else {
            store.dispatch("GenerateRoutes").then(res=>{
                console.log("=====")
                //router.addRoute(res)
                next()
            })
        }
    }else {
        if(to.path!='/login'){
            next("/login")
        }else {
            next()
        }
    }
})
