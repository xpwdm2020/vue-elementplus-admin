const TOEKN_KEY = "token"
export const getToken = function (){
    return localStorage.getItem(TOEKN_KEY)
}
export const setToken = function(token: string){
    localStorage.setItem(TOEKN_KEY,token)
}