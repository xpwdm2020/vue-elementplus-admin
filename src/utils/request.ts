import {ElMessageBox,ElMessage} from "element-plus"
import router from "../router/index"
import { getToken } from './store';
import Axios from "axios"
const axios = Axios.create({
    baseURL:"http://localhost:3000/api"
})
const whiteUrlSuffixArr = ['login','captcha']
axios.interceptors.request.use((config)=>{
    const whiteUrl = whiteUrlSuffixArr.find((item)=>config.url?.endsWith(item))
    if(!whiteUrl){
        config.headers.token = getToken()
    }
    return config
})
axios.interceptors.response.use(async (response)=>{
    if(response.status == 401){
        try {
            await ElMessageBox.confirm("登录信息已过期是否重新登录?","提示",{
                type:"warning"
            })    
            router.push({
                path:'/login'
            })
        } catch (error) {
            console.log(error)      
        }
        return Promise.reject()
    }
    if(response.status == 200){
        if(response.data.code == 400){
            ElMessage.error(response.data.message)
            return Promise.reject()
        }
        return response.data
    }
},(err)=>{
    ElMessage.error("系统异常请联系管理员！")
})
export default axios