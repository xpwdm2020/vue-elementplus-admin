import  request from "../utils/request"
export const captcha = function(){
    return request.get("/captcha")
}
/**
 * 登录接口
 * @param data 
 */
export const login = function(data: object){
    return request.post("/login",data)
}