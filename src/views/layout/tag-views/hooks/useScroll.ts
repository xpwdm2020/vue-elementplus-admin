import {onMounted, getCurrentInstance} from 'vue'
import {BScrollConstructor} from "@better-scroll/core/dist/types/BScroll";
import Bscroll from '@better-scroll/core'
function useScroll() {
       let bscroll: BScrollConstructor
       const instance = getCurrentInstance()
       function initScroll() {
           bscroll = new Bscroll(instance?.refs.scroll as HTMLElement,{
               scrollX:true,
               scrollY:false
           })
       }
       function scrollToElement(el: HTMLElement) {
           bscroll && el && bscroll.scrollToElement(el,200,true,false)
       }
       function refresh(){
           bscroll && bscroll.refresh()
       }
       onMounted(()=>{
           initScroll()
       })
        return {
           scrollToElement,
            refresh
        }
}
export default useScroll