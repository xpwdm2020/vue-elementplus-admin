import {getCurrentInstance, nextTick, ref, onMounted,onUnmounted ,isRef,Ref} from 'vue'
import {RouteLocationNormalizedLoaded, RouteRecordRaw, useRoute, useRouter} from "vue-router";
import { useStore} from 'vuex'
import path from "path";
function useTag(scrollToElement: (el: HTMLElement) => void,refresh: () => void) {
    const instance = getCurrentInstance()
    const $store = useStore()
    const $route = useRoute()
    const $router = useRouter()
    const affixTags: RouteLocationNormalizedLoaded[] = []
    /**
     * 获取isAffix是true的路由
     * @param routes
     * @param basePath
     */
    function filterAffixTag (routes: RouteRecordRaw[], basePath = '/') {
        routes.forEach(route => {
            if (route.meta && route.meta.isAffix) {
                const tagPath: string = path.resolve(basePath, route.path)
                const tag = {path: tagPath, fullPath: tagPath, meta:{...route.meta}}
                affixTags.push(tag as RouteLocationNormalizedLoaded)
            }
            if(route.children){
                filterAffixTag(route.children,path.resolve(basePath+route.path))
            }
        })
    }
    /**
     *添加tag
     */
    function addTag(){
        // eslint-disable-next-line
        $store.dispatch('addTag',$route)
       nextTick(()=>{
            refresh()
            const index = $store.getters.tags.findIndex((tag: RouteLocationNormalizedLoaded)=>$route.path === tag.path)
            if (index>-1) {
                scrollToElement(instance?.refs['scrollItem' + index] as HTMLElement)
            }
        })
    }
    /**
     *tag切换时切换页面
     * @param event
     * @param tag
     */
    function switchRoute(event: MouseEvent, tag: RouteLocationNormalizedLoaded){
        scrollToElement(event.target as HTMLElement)
        $router.push(tag.path)
    }
    /**
     *  关闭标签
     * @param index
     * @param tag
     */
    function closeTag(index: number, tag: RouteLocationNormalizedLoaded){
        $store.dispatch("delTag",tag).then(()=>{
            if($route.path === tag.path){
                const tag1 = $store.getters.tags[index]
                if(tag1){
                    $router.push(tag1.path)
                }else {
                    index = $store.getters.tags.length-1
                    const tag2 = $store.getters.tags[index]
                    console.log('tag2',tag2)
                    console.log(tag2.path)
                    $router.push(tag2.path)
                }
                nextTick(()=>{
                    scrollToElement(instance?.refs['scrollItem'+index] as HTMLElement)
                })
            }
        })
    }
    /**
     * 关闭所有标签
     */
    function closeAllTag(){
        $store.dispatch('delAllTag').then(()=>{
            const tag = $store.getters.tags[$store.getters.tags.length-1]
            $router.push(tag.path)
        })
    }
    //选中标签的index
    const  selectedIndex = ref(-1)
    //选中的标签
    const  selectedTag  = ref<RouteLocationNormalizedLoaded|null>(null)
    //控制右键菜单显示
    const visible = ref(false)
    //右键菜单打开距离屏幕左边的距离
    const left = ref('')
    //右键菜单距离顶部的距离
    const top = ref('')
    /**
     * 右键打开菜单
     * @param event
     * @param index
     * @param tag
     */
    function openMenu(event: MouseEvent, index: number , tag: RouteLocationNormalizedLoaded){
        selectedIndex.value = index
        selectedTag.value = tag
        const menuWidth = 68
        const clientWidth = window.screen.width
        let clientX = event.clientX
        if(clientWidth-clientX<menuWidth){
            clientX = clientX - menuWidth
        }
        visible.value = true
        left.value = clientX + 'px'
        top.value = event.clientY + 'px'
    }
    /**
     *关闭其他标签
     */
    function closeOtherTag(){
        $store.dispatch('delOtherTag',selectedTag.value)
    }
    /**
     *右键关闭菜单
     */
    function closeMenu(){
        visible.value = false
    }
    /**
     * 刷新当前页面
     */
    function refreshPage(){
        $store.dispatch('delCacheView',selectedTag.value).then(()=>{
            $router.replace({path:'/redirect'+$route.path,query:$route.query})
        })
    }
    /**
     * 判断标签是否总是显示
     * @param tag
     */
    function isAffix (tag: RouteLocationNormalizedLoaded | Ref<RouteLocationNormalizedLoaded>): boolean{
        if(tag === null){
            return false
        }
        if(isRef(tag)){
            return tag.value.meta && tag.value.meta.isAffix
        }
        return  tag.meta && tag.meta.isAffix
    }
    /**
     *初始化添加isAffix是true的标签
     */
    function initTag(){
        filterAffixTag($store.getters.routes)
        for (const tag of affixTags) {
            //eslint-disable-next-line
            $store.dispatch('addTag',tag)
        }
    }
    initTag()
    console.log('tags=',$store.getters.tags)
    onMounted(()=>{
        document.body.addEventListener('click',closeMenu)
    })
    onUnmounted(()=>{
        document.body.removeEventListener('click',closeMenu)
    })
    return {
        isAffix,
        switchRoute,
        closeTag,
        closeAllTag,
        closeOtherTag,
        openMenu,
        refreshPage,
        addTag,
        visible,
        left,
        top,
        selectedTag,
        selectedIndex
    }
}
export default useTag