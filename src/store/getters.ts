import {GetterTree} from "vuex"
const getters: GetterTree<any, any> ={
    collapse:state => state.collapse,
    menus:state => state.permission.menus,
    tags:state => state.tagViews.visitedViews,
    routes:state => state.permission.routes,
    cacheViews: state => state.tagViews.cacheViews
}
export default getters
