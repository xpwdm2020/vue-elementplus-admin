import {Module} from "vuex";
import {RouteLocationNormalizedLoaded} from "vue-router";
const ADD_VISITED_VIEWS = 'ADD_VISITED_VIEWS'
const DEL_VISITED_VIEWS = 'DEL_VISITED_VIEWS'
const DEL_ALL_VISITED_VIEWS = 'DEL_ALL_VISITED_VIEWS'
const DEL_OTHER_VISITED_VIEWS = 'DEL_OTHER_VISITED_VIEWS'
const ADD_CACHE_VIEWS = 'ADD_CACHE_VIEWS'
const DEL_CACHE_VIEWS = 'DEL_CACHE_VIEWS'
// eslint-disable-next-line @typescript-eslint/interface-name-prefix
interface ITagView {
    // eslint-disable-next-line
    visitedViews: RouteLocationNormalizedLoaded[];
    cacheViews: string[];
}
// eslint-disable-next-line
const tagViews: Module<ITagView, any> ={
    state:{
        visitedViews: [],
        cacheViews:[]
    },
    mutations:{
        [ADD_VISITED_VIEWS](state,tag: RouteLocationNormalizedLoaded){
            if(tag.path.startsWith('/redirect') || state.visitedViews.some((t)=>t.path === tag.path)){
                return
            }
            state.visitedViews.push( Object.assign({},tag,{meta: { ...tag.meta, title: tag.meta.title || 'no-name'}}))
        },
        [DEL_VISITED_VIEWS](state,tag: RouteLocationNormalizedLoaded){
            const index = state.visitedViews.findIndex(t=>t.path === tag.path)
            if(index>-1){
                state.visitedViews.splice(index,1)
                state.cacheViews = state.cacheViews.filter(name=> name !== tag.name)
            }
        },
        [DEL_ALL_VISITED_VIEWS](state){
            state.visitedViews = state.visitedViews.filter(t=>t.meta&&t.meta.isAffix)
        },
        [DEL_OTHER_VISITED_VIEWS](state, tag: RouteLocationNormalizedLoaded){
            state.visitedViews = state.visitedViews.filter(t=>t.meta&&t.meta.isAffix || t.path === tag.path)
        },
        [ADD_CACHE_VIEWS](state,tag: RouteLocationNormalizedLoaded){
            if(tag.name && !state.cacheViews.includes(tag.name as string)){
                state.cacheViews.push(tag.name as string)
            }
        },
        [DEL_CACHE_VIEWS](state,tag: RouteLocationNormalizedLoaded){
            state.cacheViews = state.cacheViews.filter(name=> name !== tag.name)
        }
    },
    actions:{
        addVisitedViews({commit},tag: RouteLocationNormalizedLoaded){
            commit(ADD_VISITED_VIEWS,tag)
        },
        delVisitedViews({commit},tag: RouteLocationNormalizedLoaded){
            commit(DEL_VISITED_VIEWS,tag)
            return Promise.resolve()
        },
        addTag({dispatch},tag: RouteLocationNormalizedLoaded){
            dispatch('addVisitedViews',tag)
            dispatch('addCacheViews',tag)
        },
        async delTag({dispatch},tag: RouteLocationNormalizedLoaded){
            await  dispatch('delVisitedViews',tag)
            await dispatch('delCacheView',tag)
            return Promise.resolve()
        },
        delAllTag({commit}){
            commit(DEL_ALL_VISITED_VIEWS)
            return
        },
        delOtherTag({commit},tag: RouteLocationNormalizedLoaded){
            commit(DEL_OTHER_VISITED_VIEWS,tag)
        },
        addCacheViews({commit},tag: RouteLocationNormalizedLoaded){
            commit(ADD_CACHE_VIEWS,tag)
        },
        async delCacheView({commit},tag: RouteLocationNormalizedLoaded){
           await  commit(DEL_CACHE_VIEWS,tag)
            return Promise.resolve()
        }
    }
}
export default tagViews
