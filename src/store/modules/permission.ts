import {constantRoutes} from "@/router";
import Layout from "@/views/layout/index.vue"
import {Module } from 'vuex'
import {RouteRecordRaw} from 'vue-router'
const SET_MENU = 'SET_MENU'
const SET_ROUTE = 'SET_ROUTE'
interface PermissionState {
    addRoutes?: RouteRecordRaw[];
    routes?: RouteRecordRaw[];
    menus?: RouteRecordRaw[];
}
function filterSyncMenu( routes: RouteRecordRaw[],root=false){
    for (let i = 0; i < routes.length; i++) {
        // eslint-disable-next-line
        let route = routes[i]
        if(root&&route.children?.length===1 ){
            const child: any = route.children[0]
            if(child.meta && child.meta.isRoot){
                if(!child.path.startsWith("/")){
                    child.path = route.path+ '/' + child.path
                }
                routes[i] = child
            }
        }
        if(route.children){
            filterSyncMenu(route.children)
        }
    }
}
function filterSyncRoutes(routes: RouteRecordRaw[]) {
    routes.forEach(route=>{
        if((route.component as any) == 'layout'){
            route.component = Layout
        }
        if(route.children){
            filterSyncRoutes(route.children)
        }
    })
}
const permission: Module<PermissionState,any> = {
    state:{
       addRoutes:[],
       routes:[],
       menus: []
    },
    mutations:{
        [SET_MENU](state,menus: []){
            state.menus = menus
        },
        [SET_ROUTE](state,routes: []){
            state.addRoutes = routes
            state.routes = constantRoutes.concat(routes)
        }
    },
    actions:{
        GenerateRoutes: function ({commit}) {
            return new Promise<RouteRecordRaw[]>((resolve) => {
                // eslint-disable-next-line
                let menus = JSON.parse(JSON.stringify(constantRoutes.concat([])));
                filterSyncMenu(menus, true)
                const routes = JSON.parse(JSON.stringify([]))
                filterSyncRoutes(routes)
                commit(SET_ROUTE, routes)
                commit(SET_MENU, menus)
                resolve([])
            })
        }
    }
}
export default permission
