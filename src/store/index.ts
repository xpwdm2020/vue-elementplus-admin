import { createStore ,} from 'vuex'
import permission from './modules/permission'
import tagViews from "@/store/modules/tagViews";
import getters from "@/store/getters";
export default createStore({
  state: {
    collapse:false
  },
  mutations: {
    SET_COLLAPSE(state,value: boolean){
      state.collapse = value
    }
  },
  actions: {
    setCollapse({commit},value: boolean){
      commit("SET_COLLAPSE",value)
    }
  },
  getters:getters,
  modules: {
    permission,
    tagViews
  }
})
