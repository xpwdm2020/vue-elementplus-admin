import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Layout from '../views/layout/index.vue';
export const constantRoutes: Array<RouteRecordRaw> = [
  {
    path: '/redirect',
    component: Layout,
    meta:{
      hidden: true
    },
    children: [
      {
        path: '/redirect/:path(.*)',
        component: ()=>import('@/views/redirect.vue')
      }
    ]
  },
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    redirect:'index',
    meta:{
      title: "test"
    },
    children:[
      {
        path:'/index',
        name:'index',
        component:()=>import('@/views/index/index.vue'),
        meta: {
          title: '首页',
          icon: 'iconfont icon-shouye',
          isRoot:true,
          isAffix:true
        }
      }
    ]
  },
  {
    path:'/login',
    name: 'Login',
    component:()=> import('@/views/login/login.vue'),
    meta:{
      hidden:true
    }
  },
  {
    path:'/system',
    name:'System',
    redirect:'/system/user',
    component:Layout,
    meta:{
      title:'系统管理',
      icon:'el-icon-setting',
    },
    children:[
      {
        path: 'user',
        name:'User',
        component:()=>import('@/views/system/user/index.vue'),
        meta:{
          title:'用户管理',
          icon:'el-icon-s-custom'
        }
      },
      {
        path: 'role',
        name: 'Role',
        component:()=>import('@/views/system/role/index.vue'),
        meta:{
          title: "角色管理",
          icon: 'iconfont icon-jiaoseguanli'
        }
      },
      {
        path: 'menu',
        name:'Menu',
        component:()=>import('@/views/system/menu/index.vue'),
        meta:{
          title: "菜单管理",
          icon: "iconfont icon-caidanguanli"
        }
      }
    ]
  },
  // {
  //   path:'/layout',
  //   name: 'Layout',
  //   component:()=> import('@/views/layout/index.vue')
  // }
]
const router = createRouter({
  history: createWebHashHistory(),
  routes: constantRoutes
})
export default router
