import { createApp } from 'vue'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import '@/style/reset.scss'
import '@/style/common.scss'
import './assets/font/iconfont.css'
import App from './App.vue'
import router from './router'
import store from './store'
import './permission'
console.log(router)
createApp(App).use(store).use(router).use(ElementPlus).mount('#app')
